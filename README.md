Naša spletna aplikacija bo namenjena objavljanju in odgovarjanju na vprašanja (kot npr. yahoo questions, stack overflow, ask,...).
Za objavljanje vprašanja mora biti uporabnik registriran oziroma prijavljen,
https://bitbucket.org/sp_mojstri/sp_projekt/src/master/docs/login.html
https://bitbucket.org/sp_mojstri/sp_projekt/src/master/docs/registration.html

nato pa lahko, če želi vprašanje postavi tudi anonimno.
https://bitbucket.org/sp_mojstri/sp_projekt/src/master/docs/ask.html

Vsak registrirani uporabnik ima svoj profil,
https://bitbucket.org/sp_mojstri/sp_projekt/src/master/docs/uporabniski_profil.html
kjer
je nekaj informacij o njegovih interesih, ime, njegov ugled (razmerje med všečki in nevšečki njegovih odgovorov) in po želji tudi profilna slika.
Nekaj teh podatkov si lahko ureja sam na nastavitvah računa
https://bitbucket.org/sp_mojstri/sp_projekt/src/master/docs/nastavitve_racuna.html

V iskalnik lahko uporabnik (tudi neregistriran) natipka svoje vprašanje, nato mu spletna stran vrne zadetke.

Ko izbere eno izmed vprašanj, se mu prikaže naslednje okno:
https://bitbucket.org/sp_mojstri/sp_projekt/src/master/docs/question.html

Uporabnik lahko na vsako vprašanje in odgovor na vprašanje odgovori, lahko
ureja že objavljene odgovore (in se pri tem drži nekaterih pravil) in izbriše vprašanje (če je on lastnik).
https://bitbucket.org/sp_mojstri/sp_projekt/src/master/docs/add_comment.html
https://bitbucket.org/sp_mojstri/sp_projekt/src/master/docs/edit.html
https://bitbucket.org/sp_mojstri/sp_projekt/src/master/docs/reply.html

Avtor vprašanja ima možnost označiti odgovor na njegovo vprašanje z "solved", ki pove, da je ta odgovor rešil njegovo vprašanje oz. je ta odgovor bil najboljši
(vidno v question.html)

Domača stran pa je index.html kjer je predstavljenih nekaj naključnih vprašanj.
https://bitbucket.org/sp_mojstri/sp_projekt/src/master/docs/index.html

Vsa aktualna vprasanja so vidla na zavihku trending
https://bitbucket.org/sp_mojstri/sp_projekt/src/master/docs/trending.html

----------------------------------------- 2.naloga ---------------------------------------------------------------------
#Rest klici

Method | Call | Description
--- | --- | 
`GET` | api/user | get all users
`GET` | api/user/:username | get user by username 
`POST` | api/user | add new user 
`PUT` | api/user/:username | update user
`DELETE` | api/user/:username | delete user
`GET` | api/logout | logout 
`POST` | api/login | login
`POST` | api/register | register
`GET` | api/question | get all questions


#3 različne naprave
- Google nexus 5 (majhen ekran, 360 x 460)
- iPad pro (ekran srednje velikosti, 1024 x 1366)
- PC (velik ekran, 1920 x 1080)


#User profile:
1. ####Change password
- Da upoprabnik spremeni geslo mora prvo vnesti staro geslo
- Novo geslo mora vsebovati vsaj 8 znakov, od teh mora biti vsaj ena števka, vsaj ena velika črka in vsaj ena mala črka. Torej so dovoljeni vsi znaki a-z && A-Z && 0-9
- Novo geslo mora nato vpisati še enkrat da se vnos potrdi

2. ####Change nickname
- Tu si lahko uporabnik izbere kakšnikoli vzdevek

3. ####Change experiences
- Prav tako tukaj ni omejitev pri vnosu, saj lahko uporabnik vse izkušnje ki jih ima do sedaj

#Trending page
- prikazana vprašanja z največ odgovori za časovno obdobja (dan/mesec/leto)
#Index page
- naključno prikazanih nekaj vprašanj
#Avtorizacija
1. ####prijava
- 2.naloga: frontend validacija- dolžina uporabnikškega in gesla imena >= 3
- 3.naloga: frontend/backend validacija- dolžina uporabnikškega imena >= 3, geslo mora izpoljevati enake pogoje kot pri točki `change password`
2.  ####registracija:
- 2.naloga: frontend validacija- dolžina uporabnikškega imena in gesla >= 3 , klasična validaijca če je email veljaven
- 3.naloga: frontend/backend validacija- dolžina uporabnikškega imena >= 3 , geslo mora izpoljevati enake pogoje kotpri točki `change password`, email- basic validacija 

#Search
- iskanje po naslovih vprašanj

#Ask a question
- title je obvezen, vsi vnosi so dovoljeni
- questionTags je neobvezen, vsi vnosi so dovoljeni

#Add an answer & Add a comment & Edit an answer
- obrazec ne sme biti prazen, vsi vnosi so dovoljeni
- 

---------------------------------------3.naloga---------------------------------------

1. Heroku link: https://askmeanything-sp-2018-2019.herokuapp.com/

2. Navodila za lokalni zagon apikacije:
- v korenski mapi po kloniranju najprej izvedemo ukaz npm install
- nato sledijo sledeči ukazi:
- sudo apt-get remove mongodb-org mongodb-org-server
- sudo apt-get autoremove
- sudo rm -rf /usr/bin/mongo*
- sudo rm /etc/apt/sources.list.d/mongodb*.list
- sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
- echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
- sudo apt-get update
- sudo apt-get install mongodb-org mongodb-org-server
- sudo touch /etc/init.d/mongod
- sudo apt-get install mongodb-org-server
- cd ~/workspace
- mkdir mongodb
- cd mongodb
- mkdir data
- echo 'mongod --bind_ip=$IP --dbpath=data --nojournal "$@"' > mongod
- chmod a+x mongod
- ./mongod
- cd ..
- nodemon