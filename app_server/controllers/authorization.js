var request = require('request');

var apiParameters = {
    server: 'http://localhost:' + process.env.PORT
};

if (process.env.NODE_ENV === 'production') {
    apiParameters.server = 'https://askmeanything-sp-2018-2019.herokuapp.com/';
}

var showError = function(req, res, statusCode) {
    var title, content;
    if (statusCode == 404) {
        title = '404, page cannot be found.';
        content = 'You must have requested something weird.';
    }
    else {
        title = statusCode + ', something went wrong.';
        content = 'Something, somewhere, doesnt do Its job...';
    }
    res.status(statusCode);
    res.render('inCaseOfError', {
        title: title,
        content: content
    });
};


module.exports.authFilter = function(req, res, next) {
    const username = req.session.username;
    if (username) {
        res.locals.username = username;
    }
    next();
};


module.exports.authorization = function(req, res) {
    res.render('authorization', {})
};

module.exports.login = function(req, res) {

    var path = '/api/login';
    var username = req.body.username;
    var password = req.body.password;

    var requestParameters = {
        url: apiParameters.server + path,
        method: 'POST',
        json: {
            username: username,
            password: password
        }
    };
    request(
        requestParameters,
        function(error, response, content) {
            if (error) {
                console.log("ERROR")
                showError(req, res, error.code);
            }
            else if (response.statusCode >= 400) {
                console.log("ERROR2")
                res.render('authorization', {
                    success: false,
                    message: "Wrong username or password"
                })
            }
            else {
                console.log("OKK")
                req.session.username = response.body.username;
                res.redirect('/')
            }
        });
};

module.exports.register = function(req, res) {

    var path = '/api/register';
    var username = req.body.username;
    var password = req.body.password;
    var email = req.body.email;

    var requestParameters = {
        url: apiParameters.server + path,
        method: 'POST',
        json: {
            username: username,
            password: password,
            email: email
        }
    };
    request(
        requestParameters,
        function(error, response, content) {
            if (error) {
                showError(req, res, error.code);
            }
            else if (response.statusCode >= 400) {
                res.render('authorization', {
                    success: false,
                    message: "Registration failed"
                });
            }
            else {
                res.render('authorization', {
                    success: true,
                    message: "Registration successful"
                });
            }
        });
};

module.exports.logout = function(req, res) {
    var path = '/api/logout';
    var requestParameters = {
        url: apiParameters.server + path,
        method: 'GET',
        json: {}
    };
    request(
        requestParameters,
        function(error, response, content) {
            if (error) {
                showError(req, res, error.code);
            }
            else if (response.statusCode >= 400) {
                showError(req, res, response.statusCode);
            }
            else {
                req.session.destroy();
                res.redirect('/authorization')
            }
        });
};
