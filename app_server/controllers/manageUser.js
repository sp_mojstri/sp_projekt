var profileZdenka = require('../models/profileZdenka.json');
var profileSimon = require('../models/profileSimon.json');

var request = require('request');

var apiParameters = {
    server: 'http://localhost:' + process.env.PORT
};

if (process.env.NODE_ENV === 'production') {
    apiParameters.server = 'https://askmeanything-sp-2018-2019.herokuapp.com/';
}

var showError = function(req, res, statusCode) {
    var title, content;
    if (statusCode == 404) {
        title = '404, page cannot be found.';
        content = 'You must have requested something weird.';
    }
    else {
        title = statusCode + ', something went wrong.';
        content = 'Something, somewhere, doesnt do Its job...';
    }
    res.status(statusCode);
    res.render('inCaseOfError', {
        title: title,
        content: content
    });
};

module.exports.userProfile = function(req, res) {
    var path = '/api/user/' + req.params.username;
    
    var requestParameters = {
        url: apiParameters.server + path,
        method: 'GET',
        json: {}
    };
    
    request(
        requestParameters,
        function(error, response, content) {
            if (response.statusCode === 200) {
                res.render('userProfile', content[0]);
            }
            else {
                showError(req, res, response.statusCode);
            }
        });
};

module.exports.userSettings = function(req, res) {
    var path = '/api/user/' + req.params.username;
    
    var requestParameters = {
        url: apiParameters.server + path,
        method: 'GET',
        json: {}
    };
    
    request(
        requestParameters,
        function(error, response, content) {
            if (response.statusCode === 200) {
                res.render('userSettings', content[0]);
            }
            else {
                showError(req, res, response.statusCode);
            }
        });
};


module.exports.expChange = function(req, res) {
    var path = '/api/user/' + req.params.username;
    /*var forwardedData = {
        experiences: req.body.dodajIzkusnje
    };*/
    
    
    var requestParameters = {
        url: apiParameters.server + path,
        method: 'PUT',
        json: {
            experiences: req.body.dodajIzkusnje
            }
        };
    console.log(path);
        
    request(
        requestParameters,
        function(error, response, content) {
            if (response.statusCode === 200) {
                res.redirect('/user/' + req.params.username + '/settings');
            }
            else {
                showError(req, res, response.statusCode);
            }
        });
};


module.exports.passChange = function(req, res) {
    console.log(req.body.novoGeslo1);
    var path = '/api/user/' + req.params.username;
    /*var forwardedData = {
        experiences: req.body.dodajIzkusnje
    };*/
    
    var requestParameters = {
        url: apiParameters.server + path,
        method: 'PUT',
        json: {
            password: req.body.novoGeslo1
            }
        };
    console.log(path);
        
    request(
        requestParameters,
        function(error, response, content) {
            if (response.statusCode === 200) {
                res.redirect('/user/' + req.params.username + '/settings');
            }
            else {
                showError(req, res, response.statusCode);
            }
        });
};

module.exports.avatarChange = function(req, res) {
    console.log("bb");
    var path = '/api/user/' + req.params.username;
    
    var requestParameters = {
        url: apiParameters.server + path,
        method: 'PUT',
        json: {
            avatar: req.body.linkURL
            }
        };
    console.log(path);
        
    request(
        requestParameters,
        function(error, response, content) {
            if (response.statusCode === 200) {
                res.redirect('/user/' + req.params.username + '/settings');
            }
            else {
                showError(req, res, response.statusCode);
            }
        });
};