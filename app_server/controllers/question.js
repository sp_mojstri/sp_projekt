var request = require('request');

var apiParameters = {
    server: 'http://localhost:' + process.env.PORT
};

if (process.env.NODE_ENV === 'production') {
    apiParameters.server = 'https://askmeanything-sp-2018-2019.herokuapp.com';
}

var showError = function(req, res, statusCode) {
    var title, content;
    if (statusCode == 404) {
        title = '404, page cannot be found.';
        content = 'You must have requested something weird.';
    }
    else {
        title = statusCode + ', something went wrong.';
        content = 'Something, somewhere, doesnt do Its job...';
    }
    res.status(statusCode);
    res.render('inCaseOfError', {
        title: title,
        content: content
    });
};



var showQuestion = function(req, res, content) {
    res.render('question', content);
};

module.exports.question = function(req, res) {
    var path = '/api/question/' + req.params.idQuestion;
    var requestParameters = {
        url: apiParameters.server + path,
        method: 'GET',
        json: {}
    };
    request(
        requestParameters,
        function(error, response, content) {
            if (error) {
                showError(req, res, error.statusCode);
            }
            else if (response.statusCode >= 400) {
                showError(req, res, response.statusCode);
            }
            else {
                showQuestion(req, res, content);
            }
        });
};

var showQuestionForm = function(req, res) {
    res.render('addQuestion');
};

module.exports.addQuestion = function(req, res) {
    var username = req.session.username;
    if (username == undefined) {
        res.render('authorization');
    }
    else {
        showQuestionForm(req, res);
    }
};

module.exports.saveQuestion = function(req, res) {
    var path = '/api/question';
    var username = req.session.username;

    var forwardedData = {
        user: username,
        title: req.body.title,
        anonymous: req.body.anonymous,
        questionTags: req.body.questionTags
    };
    var requestParameters = {
        url: apiParameters.server + path,
        method: 'POST',
        json: forwardedData
    };
    request(
        requestParameters,
        function(error, response, content) {
            if (error) {
                showError(req, res, error.statusCode);
            }
            else if (response.statusCode >= 400) {
                showError(req, res, response.statusCode);
            }
            else {
                dodajQ(req, res, content);
                var newQuestionPath = '/question/' + response.body._id;
                res.redirect(newQuestionPath);
            }
        });
};

var dodajQ = function(req, res, question) {
    var path = '/api/user/' + req.session.username;

    var requestParameters = {
        url: apiParameters.server + path,
        method: 'GET',
        json: {}
    };
    console.log(question);
    //PRIDOBIMO UPORABNIKA
    request(
        requestParameters,
        function(error, response, content) {
            if (response.statusCode === 200) {
                content[0].questions.push({
                    _id: question._id,
                    title: question.title,
                    author: question.author,
                    date: question.date,
                    questionTags: question.questionTags,
                    anonymous: question.anonymous,
                    numberOfAnswers: question.numberOfAnswers
                });

                //ČE JE VSE KUL NATO DODAMO UPORABNIKU VPRAŠANJE
                var path = '/api/user/' + req.session.username;
                console.log(content[0]);
                var requestParameters = {
                    url: apiParameters.server + path,
                    method: 'PUT',
                    json: {
                        questions: content[0].questions
                    }
                };

                request(
                    requestParameters,
                    function(error, response, content) {
                        if (response.statusCode === 200) {
                            //res.redirect('/user/' + req.params.username + '/settings');
                        }
                        else {
                            showError(req, res, response.statusCode);
                        }
                    });
            }
            else {
                showError(req, res, response.statusCode);
            }
        });
};

var showEditAnswerForm = function(req, res) {
    res.render('editAnswer');
};

module.exports.editAnswer = function(req, res) {
    var username = req.session.username;
    if (username == undefined) {
        res.render('authorization');
    }
    else {
        showEditAnswerForm(req, res);
    }
};

module.exports.saveEditedAnswer = function(req, res) {
    var idQuestion = req.params.idQuestion;
    var idAnswer = req.params.idAnswer;

    var path = '/api/question/' + idQuestion + '/answer/' + idAnswer;
    var forwardedData = {
        text: req.body.text
    };
    var requestParameters = {
        url: apiParameters.server + path,
        method: 'PUT',
        json: forwardedData
    };
    request(
        requestParameters,
        function(error, response, content) {
            if (error) {
                showError(req, res, error.statusCode);
            }
            else if (response.statusCode >= 400) {
                showError(req, res, response.statusCode);
            }
            else {
                res.redirect('/question/' + idQuestion);
            }
        });
};

var showAddAnswerForm = function(req, res) {
    res.render('addAnswer');
};

module.exports.addAnswer = function(req, res) {
    var username = req.session.username;
    if (username == undefined) {
        res.render('authorization');
    }
    else {
        showAddAnswerForm(req, res);
    }
};

module.exports.saveAnswer = function(req, res) {
    var idQuestion = req.params.idQuestion;
    var path = '/api/question/' + idQuestion;
    var username = req.session.username;

    var forwardedData = {
        user: username,
        text: req.body.text,
        anonymous: req.body.anonymous
    };
    var requestParameters = {
        url: apiParameters.server + path,
        method: 'POST',
        json: forwardedData
    };
    request(
        requestParameters,
        function(error, response, content) {
            if (error) {
                showError(req, res, error.statusCode);
            }
            else if (response.statusCode >= 400) {
                showError(req, res, response.statusCode);
            }
            else {
                res.redirect('/question/' + idQuestion);
            }
        });
};

module.exports.removeAnswer = function(req, res) {
    var username = req.session.username;
    if (username == undefined) {
        res.render('authorization');
    }
    else {
        var idQuestion = req.params.idQuestion;
        var idAnswer = req.params.idAnswer;
        var path = '/api/question/' + idQuestion + '/answer/' + idAnswer;

        var requestParameters = {
            user: username,
            url: apiParameters.server + path,
            method: 'DELETE',
            json: {}
        };
        request(
            requestParameters,
            function(error, response, content) {
                if (error) {
                    showError(req, res, error.statusCode);
                }
                else if (response.statusCode >= 400) {
                    showError(req, res, response.statusCode);
                }
                else {
                    res.redirect('/question/' + idQuestion);
                }
            });
    }
};

var showAddCommentForm = function(req, res) {
    res.render('addComment');
};

module.exports.addComment = function(req, res) {
    var username = req.session.username;
    if (username == undefined) {
        res.render('authorization');
    }
    else {
        showAddCommentForm(req, res);
    }
};

module.exports.saveComment = function(req, res) {
    var idAnswer = req.params.idQuestion; // ta idQuestion je ubistvu answer
    var idQuestion = req.originalUrl.substring(10, 34);
    var username = req.session.username;

    var path = '/api/question/' + idQuestion + '/answer/' + idAnswer;
    var forwardedData = {
        user: username,
        text: req.body.text,
        anonymous: req.body.anonymous
    };
    var requestParameters = {
        url: apiParameters.server + path,
        method: 'POST',
        json: forwardedData
    };
    request(
        requestParameters,
        function(error, response, content) {
            if (error) {
                showError(req, res, error.statusCode);
            }
            else if (response.statusCode >= 400) {
                showError(req, res, response.statusCode);
            }
            else {
                res.redirect('/question/' + idQuestion);
            }
        });
};

module.exports.likeAnswer = function(req, res) {
    var username = req.session.username;
    if (username == undefined) {
        res.render('authorization');
    }
    else {
        var idQuestion = req.params.idQuestion;
        var idAnswer = req.originalUrl.substring(42, 66);

        var path = '/api/question/' + idQuestion + '/answer/' + idAnswer;
        var forwardedData = {
            like: 'yes'
        };
        var requestParameters = {
            url: apiParameters.server + path,
            method: 'PUT',
            json: forwardedData
        };

        request(
            requestParameters,
            function(error, response, content) {
                if (error) {
                    showError(req, res, error.statusCode);
                }
                else if (response.statusCode >= 400) {
                    showError(req, res, response.statusCode);
                }
                else {
                    res.redirect('/question/' + idQuestion);
                }
            });
    }
};

module.exports.dislikeAnswer = function(req, res) {
    var username = req.session.username;
    if (username == undefined) {
        res.render('authorization');
    }
    else {
        var idQuestion = req.params.idQuestion;
        var idAnswer = req.originalUrl.substring(42, 66);

        var path = '/api/question/' + idQuestion + '/answer/' + idAnswer;
        var forwardedData = {
            dislike: 'yes'
        };
        var requestParameters = {
            url: apiParameters.server + path,
            method: 'PUT',
            json: forwardedData
        };
        request(
            requestParameters,
            function(error, response, content) {
                if (error) {
                    showError(req, res, error.statusCode);
                }
                else if (response.statusCode >= 400) {
                    showError(req, res, response.statusCode);
                }
                else {
                    res.redirect('/question/' + idQuestion);
                }
            });

    }
};


var showQuestions = function(req, res, content) {
    res.render('index', {
        questions: content
    });
};

module.exports.questions = function(req, res) {
    var path = '/api/question';
    var requestParameters = {
        url: apiParameters.server + path,
        method: 'GET',
        json: {},
        qs: {
            limit: 5
        }
    };
    request(
        requestParameters,
        function(error, response, content) {
            if (error) {
                showError(req, res, error.statusCode);
            }
            else if (response.statusCode == 404) { //izjemoma tak vrstni red if stavkov: ce ni rezultatov na frontendu pokazi obvestilo !
                res.render('index', {
                    message: "No questions found"
                });
            }
            else if (response.statusCode >= 400) { // pohandlej vse ostale napake
                showError(req, res, response.statusCode);
            }
            else {
                showQuestions(req, res, content);
            }
        }
    );
};


var showTrendingQuestions = function(req, res, content) {
    res.render('trending', {
        questions: content
    });
};


module.exports.trending = function(req, res) {

    var period = req.query.period;
    var path = '/api/question';
    var requestParameters = {
        url: apiParameters.server + path,
        method: 'GET',
        json: {},
        qs: {
            period: period
        }
    };
    request(
        requestParameters,
        function(error, response, content) {
            if (error) {
                showError(req, res, error.statusCode);
            }
            else if (response.statusCode === 404) { //izjemoma tak vrstni red if stavkov: ce ni rezultatov na frontendu pokazi obvestilo !
                res.render('trending', {
                    message: "No questions found for time period"
                });
            }
            else if (response.statusCode >= 400) { // pohandlej vse ostale napake
                showError(req, res, response.statusCode);
            }
            else {
                res.render('trending', {
                    questions: content
                });
            }
        }
    );
};


var showSearchedQuestions = function(req, res, content) {
    res.render('search', {
        questions: content
    });
};


module.exports.search = function(req, res) {

    var title = req.query.text;
    var path = '/api/question';
    var requestParameters = {
        url: apiParameters.server + path,
        method: 'GET',
        json: {},
        qs: {
            title: title
        }
    };
    request(
        requestParameters,
        function(error, response, content) {
            if (response.statusCode === 200) {
                showSearchedQuestions(req, res, content);
            }
            else {
                showError(req, res, response.statusCode);
            }
        }
    );
};
