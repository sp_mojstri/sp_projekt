var request = require('request');

var apiParameters = {
    server: 'http://localhost:' + process.env.PORT
};

if (process.env.NODE_ENV === 'production') {
    apiParameters.server = 'https://askmeanything-sp-2018-2019.herokuapp.com';
}

var showError = function(req, res, statusCode) {
    var title, content;
    if (statusCode == 404) {
        title = '404, page cannot be found.';
        content = 'You must have requested something weird.';
    }
    else {
        title = statusCode + ', something went wrong.';
        content = 'Something, somewhere, doesnt do Its job...';
    }
    res.status(statusCode);
    res.render('inCaseOfError', {
        title: title,
        content: content
    });
};

var showDbIndexPage = function(req, res) {
    res.render('dbPage');
};

module.exports.dbIndexPage = function(req, res) {
    showDbIndexPage(req, res);
};

module.exports.removeData = function(req, res) {
        var path = '/api/db';
        var requestParameters = {
            url: apiParameters.server + path,
            method: 'DELETE',
            json: {}
        };
        request(
            requestParameters,
            function(error, response, content) {
                if (error) {
                    showError(req, res, error.statusCode);
                }
                else if (response.statusCode >= 400) {
                    showError(req, res, response.statusCode);
                }
                else {
                    res.redirect('/db');
                }
            });
};