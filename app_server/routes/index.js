var express = require('express');
var router = express.Router();
var ctrlQuestion = require('../controllers/question');
var ctrlAuth = require('../controllers/authorization');
var ctrlUser = require('../controllers/manageUser');
var ctrlDbPage = require('../controllers/dbPage');

router.get('*', ctrlAuth.authFilter)


//TODO : controllers should read data from rest API
router.get('/user/:username', ctrlUser.userProfile);
router.get('/user/:username/settings', ctrlUser.userSettings);
router.post('/user/:username/settings/expChange', ctrlUser.expChange);
router.post('/user/:username/settings/passChange', ctrlUser.passChange);
router.post('/user/:username/settings/avatarChange', ctrlUser.avatarChange);


router.get('/', ctrlQuestion.questions);
// Maj adding /db page for deleting and inserting data
router.get('/db', ctrlDbPage.dbIndexPage);
router.get('/db/remove', ctrlDbPage.removeData);

router.get('/question/new', ctrlQuestion.addQuestion);
router.post('/question/new', ctrlQuestion.saveQuestion);
router.get('/question/:idQuestion', ctrlQuestion.question);
router.get('/question/:idQuestion/answer/new', ctrlQuestion.addAnswer);
router.post('/question/:idQuestion/answer/new', ctrlQuestion.saveAnswer);
router.get('/question/:idQuestion/answer/:idQuestion/comment/new', ctrlQuestion.addComment);
router.post('/question/:idQuestion/answer/:idQuestion/comment/new', ctrlQuestion.saveComment);
router.get('/question/:idQuestion/answer/:idAnswer/edit', ctrlQuestion.editAnswer);
router.post('/question/:idQuestion/answer/:idAnswer/edit', ctrlQuestion.saveEditedAnswer);
router.get('/question/:idQuestion/answer/:idAnswer/remove', ctrlQuestion.removeAnswer);
router.get('/question/:idQuestion/answer/:idAnswer/like', ctrlQuestion.likeAnswer);
router.get('/question/:idQuestion/answer/:idAnswer/dislike', ctrlQuestion.dislikeAnswer);

router.get('/trending', ctrlQuestion.trending);
router.get('/trending/:period', ctrlQuestion.trending);
router.get('/search', ctrlQuestion.search);


// router.get('*', ctrlAuth.authFilter);
router.post('/login', ctrlAuth.login);
router.post('/register', ctrlAuth.register);
router.get('/logout', ctrlAuth.logout);
router.get('/authorization', ctrlAuth.authorization);


module.exports = router;
