window.addEventListener('load', function() {

    function validateEmail(email) {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }

    function validateLength(test) {
        var re = /^.{3,}/;
        return re.test(test);
    }

    function validatePassword(password) {
        var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
        return re.test(password);
    }

    var inputs = document.querySelectorAll("input.auth");

    for (var i = 0; i < inputs.length; i++) {
        inputs[i].addEventListener('input', function() {
            // var validator = this.type === 'email' ? validateEmail(this.value) : validateLength(this.value);
            var validator = undefined;

            switch (this.type) {
                case 'email':
                    validator = validateEmail(this.value);
                    break;
                case 'password':
                    validator = validatePassword(this.value);
                    break;
                default:
                    validator = validateLength(this.value);
            }

            var newClass = validator ? 'has-success' : 'has-error';
            this.parentNode.className = 'input-group' + ' ' + newClass;
        }.bind(inputs[i]));
    }
});
