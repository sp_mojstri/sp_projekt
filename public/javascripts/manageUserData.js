
window.addEventListener('load', function () {
	//document.getElementById('btnChgNick').addEventListener('click', changeNickname, false);
	//document.getElementById('btnChgPass').addEventListener('click', changePassword, false);
	document.getElementById('btnChgExp').addEventListener('click', changeExperiences, false);
	var gumbProfile = document.getElementById('gumbProfile');
	
	/*alert(userIme + " " + logIme);
	if(userIme == logIme){
		gumbProfile.style.visibility = "hidden";
	}else{
		gumbProfile.style.visibility = "hidden";
	}*/
});

/*
function changeNickname() {
	var ime = document.getElementById('insertNickname');
	var obvestilo = document.getElementById('prikazNapake');
	if(ime.value == ""){
		obvestilo.innerHTML=('<div role="alert" class="alert alert-danger">' + 
        'Please fill out all the input fields!' + '</div>');
	}else{
		obvestilo.innerHTML=('<div role="alert" class="alert alert-success">' + 
        'Nickname successfully changed!' + '</div>');
		ime.value="";
	}
}*/

function changePassword() {
	var staroGeslo = document.getElementById('staroGeslo');
	var novoGeslo1 = document.getElementById('novoGeslo1');
	var novoGeslo2 = document.getElementById('novoGeslo2');
	
	var obvestiloGeslo = document.getElementById('prikazNapakeGeslo');
	
	if(validatePassword(novoGeslo1.value) == true){
		if(novoGeslo1.value == novoGeslo2.value){
			var form = document.getElementById('form-changePassword');
			alert(staroGeslo.value + " " + userGeslo);
			
			if(staroGeslo.value == userGeslo){
				obvestiloGeslo.innerHTML=('<div role="alert" class="alert alert-success">' + 
	        	'Password successfully changed!' + '</div>');
	        
				form.submit();
				
				staroGeslo="";
				novoGeslo1="";
				novoGeslo2="";
			}else{
				obvestiloGeslo.innerHTML=('<div role="alert" class="alert alert-success">' + 
	        	'Old password is not correct!' + '</div>');
			}
			return false;
		}else{
			obvestiloGeslo.innerHTML=('<div role="alert" class="alert alert-danger">' + 
        'New password and confirmed password don\'t match!' + '</div>');
        
        	return false;
		}
	}else{
		obvestiloGeslo.innerHTML=('<div role="alert" class="alert alert-danger">' + 
        'New password must have atleast 8 alfanumeric characters, contain atlest 1 number, 1 lowercase letter, 1 uppercase letter.' + '</div>');
	
		return false;
	}
}

function changeExperiences() {
	var obvestiloGeslo = document.getElementById('prikazNapakeIzkusnje');
	obvestiloGeslo.innerHTML=('<div role="alert" class="alert alert-success">' + 
        'Your experiences are successfully changed!' + '</div>');
}


function validatePassword(password) {
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    return re.test(password);
}

function submitPassCheck()
{
  return changePassword();
}
