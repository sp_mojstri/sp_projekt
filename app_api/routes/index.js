var express = require('express');
var router = express.Router();
var ctrlQuestion = require('../controllers/question');
var ctrlComment = require('../controllers/comment');
var ctrlAnswer = require('../controllers/answer');
var ctrlUser = require('../controllers/user');
var ctrlAuth = require('../controllers/auth');
var ctrlDbPage = require('../controllers/dbPage');

//question
router.get('/question', ctrlQuestion.getAllQuestions); // show question
router.get('/question/:idQuestion', ctrlQuestion.getQuestion); // show question
router.post('/question', ctrlQuestion.addQuestion); // ask a question
router.put('/question/:idQuestion', ctrlQuestion.editQuestion);
router.delete('/question/:idQuestion', ctrlQuestion.removeQuestion);
//answer
router.get('/question/:idQuestion/answer/:idAnswer', ctrlAnswer.getAnswer);
router.post('/question/:idQuestion', ctrlAnswer.addAnswer); // new answer
router.put('/question/:idQuestion/answer/:idAnswer', ctrlAnswer.editAnswer); // edit answer
router.delete('/question/:idQuestion/answer/:idAnswer', ctrlAnswer.removeAnswer); // delete answer
//comment
router.get('/question/:idQuestion/answer/:idAnswer/comment/:idComment', ctrlComment.getComment);
router.post('/question/:idQuestion/answer/:idAnswer', ctrlComment.addComment); // new comment
router.put('/question/:idQuestion/answer/:idAnswer/comment/:idComment', ctrlComment.editComment);
router.delete('/question/:idQuestion/answer/:idAnswer/comment/:idComment', ctrlComment.removeComment);

//user
router.get('/user', ctrlUser.getAllUsers);
router.post('/user', ctrlUser.addUser);
router.get('/user/:username', ctrlUser.getUserByUsername);
router.delete('/user/:username', ctrlUser.removeUserByUsername);
router.put('/user/:username', ctrlUser.editUserByUsername);
//router.put('user/:username/chgPass', ctrlUser.changePassByUsername);


//authentication
router.post('/login', ctrlAuth.login)
router.post('/register', ctrlAuth.register)
router.get('/logout', ctrlAuth.logout)

//dbpage
router.delete('/db', ctrlDbPage.deleteData);

module.exports = router;
