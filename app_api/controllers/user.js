var mongoose = require('mongoose');
var User = mongoose.model('User');

var returnJsonResponse = function(response, status, content) {
    response.status(status);
    response.json(content);
    return response;
};

var areValidUsernamePasswordEmail = function(username, password, email) {
    return true;
}

module.exports.getAllUsers = function(req, res) {
    User
        .find(function(error, users) {


            if (error) {
                returnJsonResponse(res, 500, error);
            }

            else if (users && users.length > 0) {
                returnJsonResponse(res, 200, users);
            }

            else {
                returnJsonResponse(res, 404, { "message": "Can't find users in db" });
            }
        })
}

module.exports.getUserByUsername = function(req, res) {

    const username = req.params.username;

    if (!username) {
        returnJsonResponse(res, 400, { "message": "Username must be set" })
    }

    User
        .find({ username: username }, function(error, user) {

            if (error) {
                returnJsonResponse(res, 500, error)
            }

            else if (user) {
                returnJsonResponse(res, 200, user)
            }

            else {
                returnJsonResponse(res, 404, { "message": "Can't find user with the given username." })
            }
        })
};

module.exports.addUser = function(req, res) {

    var username = req.body.username;
    var password = req.body.password;
    var email = req.body.email;

    if (!username || !password || !email) {
        returnJsonResponse(res, 400, "Bad request: not all fields set!")
    }

    //TODO: treba prevert ce so username && password && email veljavni !!.. ni dost samo na frontendu :)
    if (areValidUsernamePasswordEmail(username, password, email)) {
        User.create({
            username: username,
            password: password,
            email: email
        }, function(error, userAdded) {
            if (error) {
                returnJsonResponse(res, 500, error);
            }
            else {
                returnJsonResponse(res, 201, userAdded);
            }
        });
    }
};


/*module.exports.editUserByUsername = function(req, res) {

    const username = req.params.username;

    if (!username) {
        returnJsonResponse(res, 400, "Bad request: pathparam 'username' must be set!")

    }

    User
        .findOne({ username: username }, function(error, user) {
            //find returns array-> we need one and the only one element that function returns.. :)

            if (error) {
                returnJsonResponse(res, 500, error);
            }

            else if (user) {
                var nickname = req.body.nickname;
                var password = req.body.password;
                var email = req.body.email;
                var experience = req.body.experience;
                var avatarUrl = req.body.avatar;

                if (nickname) {
                    user.nickname = nickname;
                }
                if (password) {
                    user.password = password;
                }
                if (email) {
                    user.email = email;
                }
                if (experience) {
                    user.experience = experience;
                }
                if (avatarUrl) {
                    user.avatar = avatarUrl;
                }
                user.save(function(error, question) {
                    if (error) {
                        returnJsonResponse(res, 400, error);
                    }
                    else {
                        returnJsonResponse(res, 200, question);
                    }
                });
            }

            else {
                returnJsonResponse(res, 404, { "message": "Can't find user." });
            }
        })
};*/

//moja verzija
module.exports.editUserByUsername = function(req, res) {
  if (!req.params.username) {
    returnJsonResponse(res, 400, {
      "sporočilo": 
        "Ne najdem imena, username je obvezen parameter"
    });
    return;
  }
  User
    .find({ username: req.params.username })
    .exec(
      function(napaka, users) {
        if (!users) {
          returnJsonResponse(res, 404, {
            "sporočilo": "Ne najdem uporabnika."
          });
          return;
        } else if (napaka) {
          returnJsonResponse(res, 500, napaka);
          return;
        }
        //users = users[0];

        var password = req.body.password;
        var email = req.body.email;
        var experiences = req.body.experiences;
        var avatar = req.body.avatar;
        var likes = req.body.likes;
        var dislikes = req.body.dislikes;
        var questions = req.body.questions;
        //console.log(password + " " + email + " " + experiences + " " + avatar + " " + likes + " " + dislikes);
        
        if (password != undefined) {
            users[0].password = password;
        }
        if (email != undefined) {
            users[0].email = email;
        }
        if (experiences != undefined) {
            users[0].experiences = experiences;
            //console.log(users[0].experiences);
        }
        if (avatar != undefined) {
            users[0].avatar = avatar;
        }
        if (likes == 'yes') {
            users[0].likes = users[0].likes + 1;
        }
        else if (likes != undefined) {
            users[0].likes = likes;
        }
        if (dislikes == 'yes') {
            users[0].dislikes = users[0].dislikes + 1;
        }
        else if (dislikes != undefined) {
            users[0].dislikes = dislikes;
        }
        if (questions != undefined) {
            users[0].questions = questions;
        }
        /*console.log("---------------------------------------------");
        console.log(users[0]);
        console.log("---------------------------------------------");*/
        
        /*users.username = req.body.username;
        users.password = req.body.password;
        users.email = req.body.email;
        users.nickname = req.body.nickname;
        users.experiences = req.body.experiences;
        users.avatar = req.body.avatar;
        users.likes= req.body.likes;
        users.dislikes = req.body.dislikes;*/

        users[0].save(function(napaka, lokacija) {
          if (napaka) {
            returnJsonResponse(res, 400, napaka);
          } else {
            returnJsonResponse(res, 200, users[0]);
          }
        });
      }
    );
};


module.exports.removeUserByUsername = function(req, res) {

    const username = req.params.username;

    if (!username) {
        returnJsonResponse(res, 400, { "message": "Can't remove user, ID of user is required." });
    }


    User
        .find({ username: username })
        .remove()
        .exec(
            function(error) {
                if (error) {
                    returnJsonResponse(res, 404, error);
                }
                else {
                    returnJsonResponse(res, 204, null);
                }
            });
};
