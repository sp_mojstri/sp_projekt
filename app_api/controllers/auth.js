var mongoose = require('mongoose');
var User = mongoose.model('User');

var returnJsonResponse = function(response, status, content) {
    response.status(status);
    response.json(content);
};

var validateEmail = function(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

var validateLength = function(test) {
    var re = /^.{3,}/;
    return re.test(test);
}

function validatePassword(password) {
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    return re.test(password);
}

var isValidRegistration = function(username, password, email) {
    if (!username || !password || !email) { return false }
    else if (validateLength(username) && validatePassword(password) && validateEmail(email)) {
        return true;
    }
    else {
        return false;
    }
}


var isValidLogin = function(username, password) {
    if (!username || !password) { return false }
    else if (validateLength(username) && validatePassword(password)) {
        return true;
    }
    else {
        return false;
    }
}


module.exports.login = function(req, res) {

    var username = req.body.username;
    var password = req.body.password;
    console.log(username);
    console.log(password);

    if (isValidLogin(username, password)) {
        User.findOne({
            username: username,
            password: password
        }, function(error, user) {
            console.log("sem noter")
            if (error) {
                returnJsonResponse(res, 500, error)
            }
            else if (user) {
                returnJsonResponse(res, 200, user);
            }
            else {
                returnJsonResponse(res, 401, { message: "Wrong username or password" });
            }
        });
    }
    else {
        returnJsonResponse(res, 400, { message: "Validation failed or all fields werent set" })
    }
};

module.exports.register = function(req, res) {

    var username = req.body.username;
    var password = req.body.password;
    var email = req.body.email;

    if (isValidRegistration(username, password, email)) {
        User.findOne({
            "$or": [{
                username: username
            }, {
                email: email
            }]
        }, function(error, user) {
            if (error) {
                returnJsonResponse(res, 500, error);
            }
            else if (user) {
                returnJsonResponse(res, 400, { message: "User already exists" });
            }
            else {
                User.create({
                    username: username,
                    password: password,
                    email: email,
                    avatar: 'https://bit.ly/2Ec0yPf'
                }, function(error, user) {
                    if (error) {
                        returnJsonResponse(res, 500, error);
                    }
                    else {
                        returnJsonResponse(res, 201, user);
                    }
                });
            }
        });
    }
    else {
        returnJsonResponse(res, 400, { message: "Validation failed or all fields werent set" })
    }
};

module.exports.logout = function(req, res) {
    returnJsonResponse(res, 204, null);
};
