var mongoose = require('mongoose');
var Question = mongoose.model('Question');

var returnJsonResponse = function(response, status, content) {
    response.status(status);
    response.json(content);
};

var getDate = function() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();
    today = dd.toString() + '.' + mm.toString() + '.' + yyyy.toString();

    return today;
};


module.exports.getAnswer = function(request, response) {
    if(request.params && request.params.idQuestion && request.params.idAnswer) {
        Question
            .findById(request.params.idQuestion)
            .select('answers')
            .exec(
                function (error, question) {
                    var answer;
                    
                    if(!question) {
                        returnJsonResponse(response, 404, {
                            "message": "Can't find question with the given ID." 
                        });
                        return;
                    }
                    else if (error) {
                        returnJsonResponse(response, 500, error);
                        return;
                    }
                    
                    if(question.answers && question.answers.length > 0) {
                        answer = question.answers.id(request.params.idAnswer);
                        
                        if(!answer) {
                            returnJsonResponse(response, 404, {
                                "message": "Can't find answer with the given ID." 
                            });
                        }
                        else {
                            returnJsonResponse(response, 200, {
                                question: {
                                    title: question.title,
                                    id: request.params.idQuestion
                                },
                                answer: answer
                            });
                        }
                    }
                    else {
                        returnJsonResponse(response, 404, {
                            "message": "Can't find any answers."
                        });
                    }
                });
    }
    else {
        returnJsonResponse(response, 400, {
            "message": "Can't find document, both id's are required."
        });
    }
};


module.exports.addAnswer = function(request, response) {
    var idQuestion = request.params.idQuestion;
    if (idQuestion) {
        Question
            .findById(idQuestion)
            .select('answers')
            .exec(
                function(error, question) {
                    if(error) {
                        returnJsonResponse(response, 400, error);
                    }
                    else {
                        addAnswer(request, response, question);
                    }
                });
    }
    else {
        returnJsonResponse(response, 400, {
            "message": "Can't find question, question ID is required"
        });
    }
};


module.exports.editAnswer = function(request, response) {
    if(!request.params.idQuestion || !request.params.idAnswer) {
        returnJsonResponse(response, 400, {
            "message": "Can't find question or answer, idQuestion and idAnswer are required"
        });
        return;
    }
    Question
        .findById(request.params.idQuestion)
        .select('answers')
        .exec(
            function(error, question) {
                if(!question) {
                    returnJsonResponse(response, 404, {
                        "message": "Can't find question." 
                    });
                    return;
                } 
                else if(error) {
                    returnJsonResponse(response, 500, error);
                }
                if(question.answers && question.answers.length > 0) {
                    var currentAnswer = question.answers.id(request.params.idAnswer);
                    
                    if(!currentAnswer) {
                        returnJsonResponse(response, 404, {
                            "message": "Can't find answer." 
                        });
                    }
                    else {
                        if(request.body.text != undefined) {
                            currentAnswer.text = request.body.text;
                        }
                        if(request.body.like != undefined) {
                            currentAnswer.numberOfLikes = currentAnswer.numberOfLikes + 1;
                        }
                        if(request.body.dislike != undefined) {
                            currentAnswer.numberOfDislikes = currentAnswer.numberOfDislikes + 1;
                        }
                        if(request.body.comments != undefined) {
                            currentAnswer.comments = request.body.comments;
                        }

                        question.save(function(error, question) {
                            if(error) {
                                returnJsonResponse(response, 400, error);
                            }
                            else {
                                returnJsonResponse(response, 200, currentAnswer);
                            }
                        });
                    }
                }
                else {
                    returnJsonResponse(response, 404, {
                        "message": "Answer for editing not found." 
                    });
                }
            });
};


module.exports.removeAnswer = function(request, response) {
    if (!request.params.idQuestion || !request.params.idAnswer) {
        returnJsonResponse(response, 400, {
            "message": "Can't find question or answer, idQuestion and idAnswer required." 
        });
        return;
    }
    Question
        .findById(request.params.idQuestion)
        .exec(
            function(error, question) {
                if (!question) {
                    returnJsonResponse(response, 404, {
                        "message": "Can't find question." 
                    });
                    return;
                }
                else if (error) {
                    returnJsonResponse(response, 500, error);
                    return;
                }
                if (question.answers && question.answers.length > 0) {
                    if (!question.answers.id(request.params.idAnswer)) {
                        returnJsonResponse(response, 404, {
                            "message": "Can't find answer."
                        });
                    }
                    else {
                        question.answers.id(request.params.idAnswer).remove();
                        question.numberOfAnswers = question.answers.length;
                        question.save(function(error) {
                            if(error) {
                                returnJsonResponse(response, 500, error);
                            }
                            else {
                                returnJsonResponse(response, 204, null);
                            }
                        });
                    }
                }
                else {
                    returnJsonResponse(response, 404, {
                        "message": "There is no answer to be deleted"
                    });
                }
            });
};

var addAnswer = function(request, response, question) {
    
    if(!question) {
        returnJsonResponse(response, 404, {
            "message": "Can't find the question"
        });
    }
    else {
        var today = getDate();
        var author = undefined; // anonymous
        var comments = [];
        if (request.body.anonymous == undefined) {
            author = request.body.user;
        }
        if (request.body.author != undefined) {
            author = request.body.author;
        }
        if (request.body.comments != undefined) {
            comments = request.body.comments;
        }
        question.answers.push({
            author: author,
            date: today, 
            text: request.body.text,
            anonymous: request.body.anonymous,
            comments: comments
        });
        question.numberOfAnswers = question.answers.length;
        question.save(function(error, question) {
            var addedAnswer;
            if(error) {
                returnJsonResponse(response, 400, error);
            }
            else {
                addedAnswer = question.answers[question.answers.length - 1];
                returnJsonResponse(response, 201, addedAnswer);
            }
        });
    }
};