var mongoose = require('mongoose');
var Question = mongoose.model('Question');
var User = mongoose.model('User');
var moment = require("moment");

var returnJsonResponse = function(response, status, content) {
    response.status(status);
    response.json(content);
};

module.exports.deleteData = function(req, res) {
    console.log('here');
    Question.remove({}, function(err) { 
        if(!err) {
            console.log('collection Questions removed');   
        }
        else {
            console.log(err);
        }
    });
    
    User.remove({}, function(err) {
        if(!err) {
            console.log('collection Users removed');
        }
        else {
            console.log(err);
        }
    });
};