var mongoose = require('mongoose');
var Question = mongoose.model('Question');

var returnJsonResponse = function(response, status, content) {
    response.status(status);
    response.json(content);
};

var getDate = function() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();
    today = dd.toString() + '.' + mm.toString() + '.' + yyyy.toString();

    return today;
};

module.exports.getComment = function(request, response) {
    if(request.params && request.params.idQuestion && request.params.idAnswer && request.params.idComment) {
        Question
            .findById(request.params.idQuestion)
            .exec(
                function (error, question) {
                    var answer;
                    
                    if(!question) {
                        returnJsonResponse(response, 404, {
                            "message": "Can't find question with the given ID." 
                        });
                        return;
                    }
                    else if (error) {
                        returnJsonResponse(response, 500, error);
                        return;
                    }
                    
                    if(question.answers && question.answers.length > 0) {
                        answer = question.answers.id(request.params.idAnswer);
                        
                        if(!answer) {
                            returnJsonResponse(response, 404, {
                                "message": "Can't find answer with the given ID." 
                            });
                        }
                        else {
                            var comment;
                            
                            if(answer.comments && answer.comments.length > 0) {
                                comment = answer.comments.id(request.params.idComment);
                                
                                if(!comment) {
                                    returnJsonResponse(response, 404, {
                                        "message": "Can't find comment with the given ID." 
                                    });
                                }
                                else {
                                    returnJsonResponse(response, 200, {
                                        question: {
                                            title: question.title,
                                            id: request.params.idQuestion
                                        },
                                        answer: {
                                            text: answer.text,
                                            id: answer.id
                                        },
                                        comment: comment
                                    });
                                }
                            }
                            else {
                                returnJsonResponse(response, 404, {
                                    "message": "Can't find any comments." 
                                });
                            }
                        }
                    }
                    else {
                        returnJsonResponse(response, 404, {
                            "message": "Can't find any answers."
                        });
                    }
                });
    }
    else {
        returnJsonResponse(response, 400, {
            "message": "Can't find document, all id's are required."
        });
    }
};

module.exports.addComment = function(request, response) {
    var idQuestion = request.params.idQuestion;
    var idAnswer = request.params.idAnswer;
    if (idQuestion && idAnswer) {
        Question
            .findById(idQuestion)
            .exec(
                function(error, question) {
                    if(error) {
                        returnJsonResponse(response, 400, error);
                    }
                    else {
                        addComment(request, response, question);
                    }
                });
    }
    else {
        returnJsonResponse(response, 400, {
            "message": "Can't find question or answer,both ID is required"
        });
    }
};


module.exports.removeComment = function(request, response) {
    console.log('here');
    if(request.params && request.params.idQuestion && request.params.idAnswer && request.params.idComment) {
        Question
            .findById(request.params.idQuestion)
            .select('answers')
            .exec(
                function (error, question) {
                    var answer;
                    
                    if(!question) {
                        returnJsonResponse(response, 404, {
                            "message": "Can't find question with the given ID." 
                        });
                        return;
                    }
                    else if (error) {
                        returnJsonResponse(response, 500, error);
                        return;
                    }
                    
                    if(question.answers && question.answers.length > 0) {
                        answer = question.answers.id(request.params.idAnswer);
                        
                        if(!answer) {
                            returnJsonResponse(response, 404, {
                                "message": "Can't find answer with the given ID." 
                            });
                        }
                        else {
                            var comment;
                            
                            if(answer.comments && answer.comments.length > 0) {
                                comment = answer.comments.id(request.params.idComment);
                                
                                if(!comment) {
                                    returnJsonResponse(response, 404, {
                                        "message": "Can't find comment with the given ID." 
                                    });
                                }
                                else {
                                    answer.comments.id(request.params.idComment).remove();
                                    question.save(function(error) {
                                        if(error) {
                                            returnJsonResponse(response, 500, error);
                                        }
                                        else {
                                            returnJsonResponse(response, 204, null);
                                        }
                                    });
                                }
                            }
                            else {
                                returnJsonResponse(response, 404, {
                                    "message": "Can't find any comments." 
                                });
                            }
                        }
                    }
                    else {
                        returnJsonResponse(response, 404, {
                            "message": "Can't find any answers."
                        });
                    }
                });
    }
    else {
        returnJsonResponse(response, 400, {
            "message": "Can't find document, all id's are required."
        });
    }
};


module.exports.editComment = function(request, response) {
    if(request.params && request.params.idQuestion && request.params.idAnswer && request.params.idComment) {
        Question
            .findById(request.params.idQuestion)
            .exec(
                function (error, question) {
                    var answer;
                    
                    if(!question) {
                        returnJsonResponse(response, 404, {
                            "message": "Can't find question with the given ID." 
                        });
                        return;
                    }
                    else if (error) {
                        returnJsonResponse(response, 500, error);
                        return;
                    }
                    
                    if(question.answers && question.answers.length > 0) {
                        answer = question.answers.id(request.params.idAnswer);
                        
                        if(!answer) {
                            returnJsonResponse(response, 404, {
                                "message": "Can't find answer with the given ID." 
                            });
                        }
                        else {
                            
                            if(answer.comments && answer.comments.length > 0) {
                                var currentComment = answer.comments.id(request.params.idComment);
                                
                                if(!currentComment) {
                                    returnJsonResponse(response, 404, {
                                        "message": "Can't find comment with the given ID." 
                                    });
                                }
                                else {
                                    if (request.body.author != undefined) {
                                        currentComment.author = request.body.author;
                                    }
                                    if (request.body.text != undefined) {
                                        currentComment.text = request.body.text;
                                    }
                                    if (request.body.date != undefined) {
                                        currentComment.date = request.body.date;
                                    }
                                    
                                    question.save(function(error) {
                                        if(error) {
                                            returnJsonResponse(response, 500, error);
                                        }
                                        else {
                                            returnJsonResponse(response, 204, currentComment);
                                        }
                                    });
                                }
                            }
                            else {
                                returnJsonResponse(response, 404, {
                                    "message": "Can't find any comments." 
                                });
                            }
                        }
                    }
                    else {
                        returnJsonResponse(response, 404, {
                            "message": "Can't find any answers."
                        });
                    }
                });
    }
    else {
        returnJsonResponse(response, 400, {
            "message": "Can't find document, all id's are required."
        });
    }
};



var addComment = function(request, response, question) {
    if(!question) {
        returnJsonResponse(response, 404, {
            "message": "Can't find the question"
        });
    }
    else {
        if(question.answers && question.answers.length > 0) {
            var answer = question.answers.id(request.params.idAnswer);
            var today = getDate();
            var text;
            var author = undefined;
            if (request.body.anonymous == undefined) {
                author = request.body.user;
            }
            if (request.body.author != undefined) {
                author = request.body.author;
            }
            if (request.body.date != undefined) {
                today = request.body.date;
            }

            answer.comments.push({
                author: author,
                date: today,
                anonymous: request.body.anonymous,
                text: request.body.text
            });
            
            question.save(function(error, question) {
                var addedComment;
                if(error) {
                    returnJsonResponse(response, 400, error);
                }
                else {
                    addedComment = answer.comments[answer.comments.length - 1];
                    returnJsonResponse(response, 201, addedComment);
                }
            });
        }
        else {
            returnJsonResponse(response, 404, {
                "message": "Can't find any answers"
            });
        }
    }
};