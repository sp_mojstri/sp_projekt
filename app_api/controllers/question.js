var mongoose = require('mongoose');
var Question = mongoose.model('Question');
var User = mongoose.model('User');
var moment = require("moment");

var returnJsonResponse = function(response, status, content) {
    response.status(status);
    response.json(content);
};

var getDateIntervalForTimePeriod = function(period) {

    var begin = undefined;
    var end = undefined;

    console.log("sem noter")

    switch (period) {
        case 'day':
            begin = moment().startOf('day');
            end = moment().endOf('day');
            break;
        case 'month':
            begin = moment().startOf('month');
            end = moment().endOf('month');
            break;
        case 'year':
            begin = moment().startOf('year');
            end = moment().endOf('year');
            break;
        default:
            begin = new Date(2000, 0, 1)
            end = new Date(2030, 0, 1)
    }
    begin = new Date(begin.toISOString());
    end = new Date(end.toISOString());
    return [begin, end];
}



module.exports.getAllQuestions = function(req, res) {

    var period = req.query.period;
    var limit = req.query.limit ? parseInt(req.query.limit) : 99999;
    var startDate = getDateIntervalForTimePeriod(period)[0];
    var endDate = getDateIntervalForTimePeriod(period)[1];
    var titleRegex = req.query.title ? `.*${req.query.title}.*` : ".*";
    //ni bug :) ce je prva stran (edina uporablja limit) pol kontra sortitrej... san da na drgac zgleda k trending
    var sortMode = req.query.limit ? 1 : -1;

    Question.aggregate([
            { $match: { title: { $regex: titleRegex }, date: { $gte: startDate, $lt: endDate } } },
            { $addFields: { totalAnswers: { $size: "$answers" } } },
            { $limit: limit },
            { $sort: { totalAnswers: sortMode } },

        ],
        function(error, questions) {

            if (questions && questions.length > 0) {
                returnJsonResponse(res, 200, questions);
            }
            else if (error) {
                returnJsonResponse(res, 500, error);
            }
            else {
                returnJsonResponse(res, 404, { "message": "Can't find questions in db" });
            }
        });
};

module.exports.getQuestion = function(request, response) {
    if (request.params && request.params.idQuestion) {
        Question
            .findById(request.params.idQuestion)
            .exec(function(error, question) {
                if (!question) {
                    returnJsonResponse(response, 404, {
                        "message": "Can't find question with the given ID."
                    });
                    return;
                }
                else if (error) {
                    returnJsonResponse(response, 500, error);
                    return;
                }
                returnJsonResponse(response, 200, question);
            });
    }
    else {
        returnJsonResponse(response, 400, {
            "message": "Missing ID of the question"
        });
    }
};


module.exports.addQuestion = function(request, response) {
    var today = new Date();
    var author = undefined;
    var answers = [];
    var questionTags = [];
    if (request.body.anonymous == undefined) {
        author = request.body.user;
    }
    if (request.body.author != undefined) {
        author = request.body.author;
    }
    if (request.body.date != undefined) {
        today = request.body.date;
    }
    if (request.body.answers != undefined) {
        answers = request.body.answers;
    }
    if (request.body.questionTags != undefined) {
        questionTags = request.body.questionTags;
    }
    else {
        questionTags = request.body.questionTags.split(' ');
    }

    Question.create({
        title: request.body.title,
        author: author,
        date: today,
        questionTags: questionTags,
        anonymous: request.body.anonymous,
        answers: answers
    }, function(error, question) {
        if (error) {
            returnJsonResponse(response, 400, error);
        }
        else {
            returnJsonResponse(response, 201, question);
        }
    });
};


module.exports.editQuestion = function(request, response) {

    if (!request.params.idQuestion) {
        returnJsonResponse(response, 400, {
            "message": "Can't find question, idQuestion is required."
        });
        return;
    }
    Question
        .findById(request.params.idQuestion)
        .select('-answers -numberOfAnswers')
        .exec(
            function(error, question) {
                if (!question) {
                    returnJsonResponse(response, 404, {
                        "message": "Can't find question."
                    });
                    return;
                }
                else if (error) {
                    returnJsonResponse(response, 500, error);
                }

                if (request.body.title != undefined) {
                    question.title = request.body.title;
                }
                if (request.body.date != undefined) {
                    question.date = request.body.date;
                }
                if (request.body.author != undefined) {
                    question.author = request.body.author;
                }
                if (request.body.questionTags != undefined) {
                    question.questionTags = request.body.questionTags;
                }
                if (request.body.anonymous != undefined) {
                    question.anonymous = request.body.anonymous;
                }
                if (request.body.answers != undefined) {
                    question.answers = request.body.answers;
                }

                question.save(function(error, question) {
                    if (error) {
                        returnJsonResponse(response, 400, error);
                    }
                    else {
                        returnJsonResponse(response, 200, question);
                    }
                });
            });
};


module.exports.removeQuestion = function(request, response) {
    var idQuestion = request.params.idQuestion;

    if (idQuestion) {
        Question
            .findByIdAndRemove(idQuestion)
            .exec(
                function(error, question) {
                    if (error) {
                        returnJsonResponse(response, 404, error);
                        return;
                    }
                    returnJsonResponse(response, 204, null);
                });
    }
    else {
        returnJsonResponse(response, 400, {
            "message": "Can't find question, idQuestion is required."
        });
    }
};
