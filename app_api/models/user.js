var mongoose = require('mongoose');
var QuestionsSchema = require('mongoose').model('Question').schema;


// var userSchema = new mongoose.Schema({
//     username: { type: String, required: true },
//     password: { type: String, required: true },
//     email: { type: String, required: true },
//     experience: String,
//     likes: { type: Number, "default": 0 },
//     dislikes: { type: Number, "default": 0 },
//     avatar: { data: Buffer, contentType: String },
//     questions: [question.questionSchema],
//     answers: [question.answerSchema]
// });

var userSchema = new mongoose.Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    email: { type: String, required: true },
    experiences: String,
    likes: { type: Number, "default": 0 },
    dislikes: { type: Number, "default": 0 },
    questions: [QuestionsSchema],
    avatar: {type: String, "default": 'https://bit.ly/2Ec10Np'}
});

mongoose.model('User', userSchema, 'Users');


//vse se da zračunat samo Best Answer se še doda
/*
db.User.save(
{
    username: 'Simon Pribiti',
    password: 'aa11AAbb',
    email: 'simon.pribiti4Eva@hotmail.com',
    experiences: 'When I was younger I beat my meat, now I beat my kid.',
    likes: 3,
    dislikes: 420,
    numberOfBestAnswers: 1,
    questions: [{
                    title: "How to make a website as fast as possible?",
                    author: "Andrea",
                    date: "23.8.2018",
                    questionTags: ["how", "help"],
                    anonymous: false,
                    numberOfAnswers: 2,
                    answers: [{
                        author: "Zdenka",
                        date: "25.8.2018",
                        text: "The best way to do that is to watch youtube videos, but remember to always know exactly what your page is going to look like.",
                        numberOfLikes: 99,
                        numberOfDislikes: 13,
                        anonymous: false
                    }]
                }],
    avatar: 'https://bit.ly/2Ec10Np'
})*/

/*
db.User.update({
    username: "aaa"   
}, {
    $push: {
        questions: {
            title: "How to make a website as fast as possible?",
            author: "Andrea",
            date: "23.8.2018",
            questionTags: ["how", "help"],
            anonymous: false,
            numberOfAnswers: 2,
            answers: [{
                author: "Zdenka",
                date: "25.8.2018",
                text: "The best way to do that is to watch youtube videos, but remember to always know exactly what your page is going to look like.",
                numberOfLikes: 99,
                numberOfDislikes: 13,
                anonymous: false
            }]
        }
    }
})
*/

/*
db.Questions.save(
{
    title: "How to make a website as fast as possible?",
    author: "Andrea",
    date: "23.8.2018",
    questionTags: ["how", "help"],
    anonymous: false,
    numberOfAnswers: 2,
    answers: [{
        author: "Zdenka",
        date: "25.8.2018",
        text: "The best way to do that is to watch youtube videos, but remember to always know exactly what your page is going to look like.",
        numberOfLikes: 99,
        numberOfDislikes: 13,
        anonymous: false
    }]
})


var questionSchema = new mongoose.Schema({
    title: "aa",
    author: "bb", //zaenkrat, mitja naredi shemo za uporabnika
    date: "1.2.2012",
    numberOfAnswers: 5,
});*/
