var mongoose = require('mongoose');

var commentSchema = new mongoose.Schema({
    author: String, //Zaenkrat, mitja naredi shemo za uporabnika
    date: Date,
    anonymous: String,
    text: String
});

var answerSchema = new mongoose.Schema({
    author: String, //Zaenkrat, mitja naredi shemo za uporabnika
    date: Date,
    text: { type: String, required: true },
    numberOfLikes: { type: Number, "default": 0 },
    numberOfDislikes: { type: Number, "default": 0 },
    anonymous: String, // On / Off
    comments: [commentSchema]
});

var questionSchema = new mongoose.Schema({
    title: { type: String, required: true },
    author: String, //zaenkrat, mitja naredi shemo za uporabnika
    date: Date,
    questionTags: [String],
    anonymous: String,
    numberOfAnswers: { type: Number, "default": 0 },
    answers: [answerSchema]
});

mongoose.model('Question', questionSchema, 'Questions');
